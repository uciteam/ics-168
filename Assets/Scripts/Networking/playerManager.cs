﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text; 

public class otherPlayer{
	public int playerNumber { get; set; }
	public float initialX { get; set; }
	public float initialY { get; set; }
	public GameObject playerPrefab {get; set; }
	public otherPlayer(int otherPlayerNumber, GameObject otherPlayerPrefab, float passedX, float passedY){
		playerNumber = otherPlayerNumber;
		playerPrefab = otherPlayerPrefab;
		initialX = passedX;
		initialY = passedY;
	}
}

public class playerManager : MonoBehaviour {

	// Use this for initialization
	public GameObject bomb;
	
	//public  Transform otherPlayer;
	static string[] stringSeparators = new string[] { "<EOF>" };
	//private GameObject otherPlayer;
	private StateObject receiveObject;
	private int playerNumber;

	private bool killPlayerFlag = false;
	private int playerToKill;

	// --------- new stuff
	private bool gameSetUp = false;
	private bool gameReady = false;
	private bool playerMoved = false;
	private bool bombPlaced = false;
	private Vector3 bombPosition;
	private ArrayList playerPositions = new ArrayList();
	private ArrayList otherPlayers = new ArrayList();
	private Vector2 playerMovedNumberAndIndex = new Vector2();
	//--------------------------

	private ScoreBoard score;
	void Start(){
		//otherPlayer.GetComponent<Renderer> ().enabled = false;
		score = (ScoreBoard)FindObjectOfType (typeof(ScoreBoard));
	}
	void FixedUpdate(){

		if (gameSetUp && !gameReady) {
			foreach (Vector3 pos in playerPositions) {
				GameObject tempPlayer = (GameObject)Instantiate (Resources.Load ("playerTwoPrefab"), new Vector3 (pos.y, pos.z, -1), Quaternion.identity);
				otherPlayer temp = new otherPlayer((int)pos.x, tempPlayer, pos.y, pos.z);
				otherPlayers.Add (temp);
			}
			Debug.Log ("OMFFFGGGG WTFFFFFF" + otherPlayers.Count);
			gameReady = true;
		} else if (gameReady) {
			if(playerMoved){
				movePlayers();
				playerMoved = false;
			}
			if(bombPlaced){
				Instantiate(bomb, bombPosition, Quaternion.identity);
				bombPlaced = false;
			}
			if(killPlayerFlag){
				for(int i = 0; i < otherPlayers.Count; i++){
					otherPlayer temp = (otherPlayer)otherPlayers[i];
					if(temp.playerNumber == playerToKill){
						temp.playerPrefab.GetComponent<SpriteRenderer>().enabled = false;
						break;
					}
				}
				killPlayerFlag = false;
			}
		}
	}
	public void setSend(StateObject receiveObjectPassed, int localPlayerNumber, bool firstPlayerToJoin, JSONObject initialMessage){
		receiveObject = receiveObjectPassed;
		playerNumber = localPlayerNumber;
		if (!firstPlayerToJoin) {
			instancePlayers(initialMessage);
		}
		foreach (JSONObject user in initialMessage["users"].list)
		{
			if((int)user["player"]["playerNumber"].n == playerNumber){
				score.updateScore(user["name"].str, localPlayerNumber);
			}
			
		}
		Receive(receiveObject);
	}
	private void Receive(StateObject state){
		try{
			Socket client = state.workSocket;
			Debug.Log("OK THIS IS BULLSHIT 1.0");
			// Begin receiving the data from the remote device.
			client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
			Debug.Log("OK THIS IS BULLSHIT 2.0");
		}
		catch (Exception e){
			Console.WriteLine(e.ToString());
		}
	}
	private void ReceiveCallback(IAsyncResult ar){
		try{
			// Retrieve the state object and the client socket from the asynchronous state object.
			StateObject state = (StateObject) ar.AsyncState;
			Socket client = state.workSocket;
			// Read data from the remote device.
			int bytesRead = client.EndReceive(ar);
			
			if (bytesRead > 0){
				state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
				string content = state.sb.ToString();
				String[] message = content.Split(stringSeparators, StringSplitOptions.None);
				
				if (message.Length > 0){
					state.receiveDone.Set();
					state.response = message[message.Length - 2];
					JSONObject json = new JSONObject(message[message.Length - 2]);
					Debug.Log("Json from server received: " + json);

					if(!gameSetUp){
						switch(json["type"].str){
						case "playerConnect":
							Vector3 tempPlayer = new Vector3((int)json["user"]["player"]["playerNumber"].n, (float)json["user"]["player"]["xPos"].n, (float)json["user"]["player"]["yPos"].n);

							score.updateScore (json["user"]["name"].str, (int)json["user"]["player"]["playerNumber"].n);

							playerPositions.Add(tempPlayer);
							break;
						case "playerDisconnect":
							score.deleteScore(json["user"]["name"].str);
							break;
						case "gamestate":
							if(String.Compare(json["state"].str, "started") == 0){
								gameSetUp = true;
							}
							break;
						}
					}
					else{
						switch(json["type"].str){
						case "playerUpdate":
							if((int)json["player"]["playerNumber"].n != playerNumber && gameReady){
								if(!json["player"]["alive"].b){
									killPlayer((int)json["player"]["playerNumber"].n);
								}else{
									for(int i = 0; i < playerPositions.Count; i++){
										if((int)json["player"]["playerNumber"].n == (int)((Vector3)playerPositions[i]).x){
											playerPositions[i] = new Vector3((int)json["player"]["playerNumber"].n, (float)json["player"]["xPos"].n, (float)json["player"]["yPos"].n);
											playerMovedNumberAndIndex = new Vector2((int)json["player"]["playerNumber"].n, i);
											playerMoved = true;
										}
									}
								}
							}
							break;
						case "bomb":
							bombPosition = new Vector3((float)json["bomb"]["xPos"].n, (float)json["bomb"]["yPos"].n);
							bombPlaced = true;
							break;
						case "gamestate":
							if(String.Compare(json["state"].str, "ended") == 0){
								foreach (JSONObject user in json["users"].list)
								{
									score.updateScoreFinal(user["name"].str, (int)user["player"]["score"].n);
								}
							}
							break;
						case "playerDisconnect":
							score.deleteScore(json["user"]["name"].str);
							killPlayer((int)json["user"]["player"]["playerNumber"].n);
							break;
						}
					}
				}
			}
			else{
				Debug.Log("Connection close has been requested.");
			}
			client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
			state.receiveDone.WaitOne(5000);
		}
		catch (Exception e){
			Debug.Log(e.ToString());
		}
	}
	public void instancePlayers(JSONObject jsonpassed){
		foreach (JSONObject user in jsonpassed["users"].list)
		{
			if((int)user["player"]["playerNumber"].n != playerNumber){
				Vector3 tempPlayer = new Vector3( (int)user["player"]["playerNumber"].n, (float)user["player"]["xPos"].n, (float)user["player"]["yPos"].n);
				playerPositions.Add(tempPlayer);
				score.updateScore (user["name"].str, (int)user["player"]["playerNumber"].n);
			}

		}
		gameSetUp = true;
	}
	public void movePlayers(){
		for(int i = 0; i < otherPlayers.Count; i++){
			otherPlayer temp = (otherPlayer)otherPlayers[i];
			if(temp.playerNumber == (int)playerMovedNumberAndIndex.x){
				Vector3 tempPos = (Vector3)playerPositions[(int)playerMovedNumberAndIndex.y];
				temp.playerPrefab.transform.position = new Vector3 (tempPos.y, tempPos.z, -1);
			}
		}
		playerMoved = false;
	}
	private void killPlayer(int playerToKillPassed){
		playerToKill = playerToKillPassed;
		killPlayerFlag = true;
	}
}
