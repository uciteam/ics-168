﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text; 

// State object for receiving data from remote device.
public class StateObject
{
	// Client socket.
	public Socket workSocket = null;
	// Size of receive buffer.
	public const int BufferSize = 256;
	// Receive buffer.
	public byte[] buffer = new byte[BufferSize];
	// Received data string.
	public StringBuilder sb = new StringBuilder();
	// AutoResetEvent instances signal completion.
	public AutoResetEvent connectDone = new AutoResetEvent(false);
	public AutoResetEvent receiveDone = new AutoResetEvent(false);
	public AutoResetEvent sendDone = new AutoResetEvent(false);
	// The response from the remote device.
	public String response = String.Empty;
}

public class Client : MonoBehaviour
{
	private string username, session, password;
	// The port number for the remote device.
	private const int port = 11000;
	//Separators for server messages
	static string[] stringSeparators = new string[] { "<EOF>" };
	//Our current Variables
	private bool windowCleared = false;
	private int playerNumber;
	//Objects From Scripts
	private LoginScreen loginScreen;
	private PlayerMovement player;
	private playerManager otherPlayer;
	private Lobby chatScreen;
	private Vector3 initialPosition;

	private bool firstToJoin = true;

	private bool socketCreated = false;
	public bool onChat = false;

	private JSONObject initialMessage;

	private StateObject sendStateObject, receiveStateObject;
	
	void Start()
	{
		player = (PlayerMovement)FindObjectOfType (typeof(PlayerMovement));
		otherPlayer = (playerManager)FindObjectOfType (typeof(playerManager));
		chatScreen = (Lobby)FindObjectOfType (typeof(Lobby));
	}
	
	private void StartClient()
	{
		// Connect to a remote device.
		loginScreen = (LoginScreen)FindObjectOfType (typeof(LoginScreen));
		try
		{
			// Establish the remote endpoint for the socket.
			IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
			//IPAddress ipAddress = ipHostInfo.AddressList[0];
			IPAddress ipAddress = IPAddress.Parse("192.168.0.21");
			IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);
			// Create a TCP/IP socket.
			Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			//Create StateObjects
			sendStateObject = new StateObject();
			receiveStateObject = new StateObject();
			
			sendStateObject.workSocket = client;
			receiveStateObject.workSocket = client;
			// Connect to the remote endpoint.
			client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), sendStateObject);
			// Waits for 5 seconds for connection to be done
			sendStateObject.connectDone.WaitOne(5000);
			// Send test data to the remote device.
			Send(client, "{type: 'login', user: {name: '" + username + "', password: '" + password + "', session: '" + session + "'}}<EOF>", sendStateObject);
			sendStateObject.sendDone.WaitOne(5000);
			// Receive the response from the remote device.
			Receive(receiveStateObject);
			receiveStateObject.receiveDone.WaitOne(5000);

			socketCreated = true;
		}
		catch (Exception e){
			Debug.Log(e.ToString());
		}
	}
	
	private static void ConnectCallback(IAsyncResult ar)
	{
		try
		{
			// Create the state object.
			StateObject state = (StateObject)ar.AsyncState;
			// Retrieve the socket from the state object.
			Socket client = state.workSocket;
			// Complete the connection.
			client.EndConnect(ar);
			// Signal that the connection has been made.
			state.connectDone.Set();
		}
		catch (Exception e)
		{
			Debug.Log(e.ToString());
		}
	}
	
	private void Receive(StateObject state)
	{
		try
		{
			Socket client = state.workSocket;
			// Begin receiving the data from the remote device.
			if(!onChat)
				client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReceiveCallback), state);
			else
				client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(lobbyCallBack), state);
		}
		catch (Exception e)
		{
			Console.WriteLine(e.ToString());
		}
	}
	
	private void ReceiveCallback(IAsyncResult ar)
	{
		try
		{
			// Retrieve the state object and the client socket from the asynchronous state object.
			StateObject state = (StateObject) ar.AsyncState;
			Socket client = state.workSocket;
			// Read data from the remote device.
			int bytesRead = client.EndReceive(ar);
			
			if (bytesRead > 0)
			{
				state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
				string content = state.sb.ToString();
				String[] message = content.Split(stringSeparators, StringSplitOptions.None);
				
				if (message.Length > 0){
					state.receiveDone.Set();
					state.response = message[0];

					JSONObject json = new JSONObject(message[0]);

					Debug.Log("Json from message[0] received: " + json);


					if ((String.Compare(json["type"].str, "playerConnect")) == 0 && !windowCleared){
						playerNumber = (int) json["user"]["player"]["playerNumber"].n;
						initialPosition = new Vector3(json["user"]["player"]["xPos"].n, json["user"]["player"]["yPos"].n, -1);
						//Debug.LogError(json["user"]["player"]["xPos"].n);
						if(json["existingUsers"].list.ToArray().Length != 0){
							foreach (JSONObject user in json["existingUsers"].list){
								chatScreen.connect((int)user["player"]["playerNumber"].n, user["name"].str);
							}
						}
						loginScreen.clearWindow();
						chatScreen.connect(playerNumber, json["user"]["name"].str);
						chatScreen.showWindow();
						onChat = true;
						client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(lobbyCallBack), state);
					}
				
				}
			}
			else
			{
				Debug.Log("Connection close has been requested.");
			}
		}
		catch (Exception e)
		{
			Debug.Log(e.ToString());
		}
	}

	private void lobbyCallBack(IAsyncResult ar)
	{
		Debug.Log ("Got a message");
		try
		{
			// Retrieve the state object and the client socket from the asynchronous state object.
			StateObject state = (StateObject) ar.AsyncState;
			Socket client = state.workSocket;
			// Read data from the remote device.
			int bytesRead = client.EndReceive(ar);
			
			if (bytesRead > 0)
			{
				state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));
				string content = state.sb.ToString();
				String[] message = content.Split(stringSeparators, StringSplitOptions.None);
				
				if (message.Length > 0){
					state.receiveDone.Set();
					state.response = message[0];
					
					
					JSONObject json = new JSONObject(message[message.Length - 2]);
					
					Debug.Log("Json from message[lobby] received: " + json);
					
					switch(json["type"].str){
					case "gamestate":
						firstToJoin = false;
						initialMessage = json;
						chatScreen.bitchAssNigga();
						setupScriptStateObjects();
						break;
					case "playerReady":
						chatScreen.playerReady((int)json["user"]["player"]["playerNumber"].n);
						client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(lobbyCallBack), state);
						break;
					case "playerUnready":
						chatScreen.playerUnready((int)json["user"]["player"]["playerNumber"].n);
						client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(lobbyCallBack), state);
						break;
					case "playerConnect":

						chatScreen.connect((int)json["user"]["player"]["playerNumber"].n, json["user"]["name"].str);
						client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(lobbyCallBack), state);
						break;
					case "chat":
						String messageSent = json["message"].str;
						String nameSent = json["user"]["name"].str;
						chatScreen.printMessage(nameSent, messageSent);
						client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(lobbyCallBack), state);
						break;
					
					}
				}
			}
			else
			{
				Debug.Log("Connection close has been requested.");
			}
		}
		catch (Exception e)
		{
			Debug.Log(e.ToString());
		}
	}

	private void Send(Socket client, String data, StateObject so)
	{
		// Convert the string data to byte data using ASCII encoding.
		byte[] byteData = Encoding.ASCII.GetBytes(data);
		// Begin sending the data to the remote device.
		client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), so);
	}
	
	private static void SendCallback(IAsyncResult ar)
	{
		try
		{
			// Retrieve the socket from the state object.
			StateObject so = (StateObject) ar.AsyncState;
			Socket client = so.workSocket;
			// Complete sending the data to the remote device.
			int bytesSent = client.EndSend(ar);
			// Signal that all bytes have been sent.
			so.sendDone.Set();
		}
		catch (Exception e)
		{
			Debug.Log(e.ToString());
		}
	}
	
	public void start(string usernamePassed, string passwordPassed, string sessionPassed)
	{
		username = usernamePassed;
		password = passwordPassed;
		session = sessionPassed;
		StartClient();
	}
	
	private void setupScriptStateObjects()
	{
		Debug.Log ("first");
		player.setSend (sendStateObject, playerNumber, session, initialPosition);
		otherPlayer.setSend (sendStateObject, playerNumber, firstToJoin, initialMessage);
		Debug.Log ("second");
		Debug.Log ("third");
	}
	
	void OnApplicationQuit()
	{
		if (socketCreated) {
			Send (sendStateObject.workSocket, "{type: 'playerDisconnect'}<EOF>", sendStateObject);
			receiveStateObject.workSocket.Shutdown(SocketShutdown.Both);
			//sendStateObject.workSocket.Shutdown (SocketShutdown.Both);
			receiveStateObject.workSocket.Close();
			//sendStateObject.workSocket.Close ();
		}
	}
	public void playerReady(){
		Send(sendStateObject.workSocket, "{type: 'playerReady', user: {name: '" + username + "', session: '"+ session + "'}}<EOF>", sendStateObject);
		//sendStateObject.sendDone.WaitOne(5000);
	}
	public void playerUnready(){
		Send(sendStateObject.workSocket, "{type: 'playerUnready', user: {name: '" + username  + "', session: '"+ session + "'}}<EOF>", sendStateObject);
		//sendStateObject.sendDone.WaitOne(5000);
	}
	public void sendMessage(String message){
		Send(sendStateObject.workSocket, "{type: 'chat', user: {name: '" + username  + "', session: '" + session + "'}, message: '" + message +"'}<EOF>", sendStateObject);
		//sendStateObject.sendDone.WaitOne(5000);
	}

}
