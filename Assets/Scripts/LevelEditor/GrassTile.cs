﻿using UnityEngine;
using System.Collections;

public class GrassTile : MonoBehaviour {

	// Use this for initialization
	public Sprite grassSprite;

	void AddDamage(){
		GetComponent<SpriteRenderer> ().sprite = grassSprite;
	}
}
