﻿using UnityEngine;
using System.Collections;

public class Grid : MonoBehaviour {

	// Use this for initialization

	private float ScreenSize = 800.0f;

	public float width = 10.0f;
	public float height = 10.0f;

	public Transform tilePrefab;
	public TileSet tileSet;

	public Color color = Color.white;

	void OnDrawGizmos(){
		Vector3 pos = Camera.current.transform.position;
		Gizmos.color = this.color;

		for(float y = pos.y - ScreenSize; y < pos.y + ScreenSize; y += this.height){
			Gizmos.DrawLine(new Vector3(-ScreenSize, Mathf.Floor(y/height)*height, 0.0f), 
			                new Vector3(ScreenSize, Mathf.Floor(y/height)*height, 0.0f));
		}

		for(float x = pos.x - ScreenSize; x < pos.x + ScreenSize; x += this.width){
			Gizmos.DrawLine(new Vector3(Mathf.Floor(x/width)*width, -ScreenSize, 0.0f), 
			                new Vector3(Mathf.Floor(x/width)*width, ScreenSize, 0.0f));
		}
	
	}
}
