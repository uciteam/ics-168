﻿using UnityEngine;
using System.Collections;

public class PlayerAnimation : MonoBehaviour {
	
	private Animator anim;
	private bool diag = false;
	private float movex = 0f;
	private float movey = 0f;
	private int lastDirection = 0;
	/* 0 = left
	 * 1 = up
	 * 2 = right
	 * 3 = down
	 */

	void Start () {
		anim = GetComponent<Animator>();
	}
	void Update () {
		anim.SetBool("Up_Movement", false);
		anim.SetBool("Down_Movement", false);
		anim.SetBool("Left_Movement", false);
		anim.SetBool ("Right_Movement", false);
		diag = false;
		movex = Input.GetAxis ("Horizontal");
		movey = Input.GetAxis ("Vertical");
		
		if ( movex < 0 && movey > 0) {
			anim.SetBool ("Left_Movement", true);
			lastDirection = 0;
			anim.SetBool ("Up_Idle", false);
			anim.SetBool ("Down_Idle", false);
			anim.SetBool ("Left_Idle", false);
			anim.SetBool ("Right_Idle", false);
			diag = true; 
		} else if (movex < 0 && movey < 0) {
			anim.SetBool ("Left_Movement", true);
			lastDirection = 0;
			anim.SetBool ("Up_Idle", false);
			anim.SetBool ("Down_Idle", false);
			anim.SetBool ("Left_Idle", false);
			anim.SetBool ("Right_Idle", false);
			diag = true; 
		} else if (movex > 0 && movey > 0) {
			anim.SetBool ("Right_Movement", true);
			lastDirection = 2;
			anim.SetBool ("Up_Idle", false);
			anim.SetBool ("Down_Idle", false);
			anim.SetBool ("Left_Idle", false);
			anim.SetBool ("Right_Idle", false);
			diag = true; 
		} else if (movex > 0 && movey < 0) {
			anim.SetBool("Right_Movement", true);
			lastDirection = 2;
			anim.SetBool("Up_Idle", false);
			anim.SetBool("Down_Idle", false);
			anim.SetBool("Left_Idle", false);
			anim.SetBool("Right_Idle", false);
			diag = true; 
		}
		
		if(movex > 0) {
			if(diag == false) {
				anim.SetBool("Right_Movement", true);
				lastDirection = 2;
			}
			anim.SetBool("Up_Idle", false);
			anim.SetBool("Down_Idle", false);
			anim.SetBool("Left_Idle", false);
			anim.SetBool("Right_Idle", false);
		}
		if(movex < 0) {
			if(diag == false) {
				lastDirection = 0;
				anim.SetBool("Left_Movement", true);
			}
			anim.SetBool("Up_Idle", false);
			anim.SetBool("Down_Idle", false);
			anim.SetBool("Left_Idle", false);
			anim.SetBool("Right_Idle", false);
		}
		if(movey < 0) {
			if(diag == false) {
				anim.SetBool("Down_Movement", true);
				lastDirection = 3;
			}
			anim.SetBool("Up_Idle", false);
			anim.SetBool("Down_Idle", false);
			anim.SetBool("Left_Idle", false);
			anim.SetBool("Right_Idle", false);
		}
		if(movey > 0) {
			if(diag == false) {
				anim.SetBool("Up_Movement", true);
				lastDirection = 1;
			}
			anim.SetBool("Up_Idle", false);
			anim.SetBool("Down_Idle", false);
			anim.SetBool("Left_Idle", false);
			anim.SetBool("Right_Idle", false);
		}

		if (movex == 0 && movey == 0) {
			switch(lastDirection){
			case 0: 
				anim.SetBool("Left_Idle", true);
				break;
			case 1: 
				anim.SetBool("Up_Idle", true);
				break;
			case 2: 
				anim.SetBool("Right_Idle", true);
				break;
			case 3: 
				anim.SetBool("Down_Idle", true);
				break;
			}
		}
	}
}
