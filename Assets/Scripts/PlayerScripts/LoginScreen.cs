﻿using UnityEngine;
using System.Collections;

public class LoginScreen : MonoBehaviour {

	private string username = string.Empty;
	private string password = string.Empty;
	private string session = string.Empty;

	private bool windowOn = true;

	public GUIStyle myStyle;

	private Rect windowRect = new Rect(0, 0, Screen.width, Screen.height);

	private Client userClient;
	
	void OnGUI() {
		if(windowOn)
			GUI.Window (0, windowRect, windowFunction, "Login", myStyle);
			GUI.color = Color.red;
	}

	void Update(){
		if (!windowOn) {
			//Application.LoadLevel(1);
		}
	}
	void windowFunction (int windowID) {
		username = GUI.TextField (new Rect (Screen.width / 3, Screen.height / 7, Screen.width / 3, Screen.height / 10), username, 10);
		password = GUI.PasswordField (new Rect (Screen.width / 3, 2 * Screen.height / 5, Screen.width / 3, Screen.height / 10), password, "*"[0], 10);
		session = GUI.TextField (new Rect (Screen.width / 3, 2 * Screen.height / 3, Screen.width / 3, Screen.height / 10), session, 10);

		if(GUI.Button(new Rect(Screen.width/2, 4 * Screen.height/5, Screen.width/8, Screen.height/8), "Login")) {
			userClient = (Client)FindObjectOfType (typeof(Client));
			Debug.Log("Before hash" + password);
			// Not sure how to decrypt this and check if the password is actually the same.
			//password = Md5Sum(password);
			Debug.Log("After hash" + password);
			userClient.start(username, password, session);
		}

		GUI.Label (new Rect(Screen.width/3, 8 * Screen.height/100, Screen.width/5, Screen.height/8), "Username");
		GUI.Label (new Rect(Screen.width/3, 35 * Screen.height/100, Screen.width/5, Screen.height/8), "Password");
		GUI.Label (new Rect(Screen.width/3, 62 * Screen.height/100, Screen.width/8, Screen.height/8), "Session");
	}

	public void clearWindow() {
		Debug.Log ("Welcome to Bomberman");
		windowOn = false;
	}

	public string Md5Sum(string strToEncrypt)
	{
		System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
		byte[] bytes = ue.GetBytes(strToEncrypt);
		
		// encrypt bytes
		System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
		byte[] hashBytes = md5.ComputeHash(bytes);
		
		// Convert the encrypted bytes back to a string (base 16)
		string hashString = "";
		
		for (int i = 0; i < hashBytes.Length; i++)
		{
			hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
		}
		
		return hashString.PadLeft(32, '0');
	}
}
