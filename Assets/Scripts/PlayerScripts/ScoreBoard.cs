﻿using UnityEngine;
using System.Collections;

public class ScoreBoard : MonoBehaviour {

	private bool windowOn = false;
	
	private Rect windowRect = new Rect(0, 0, Screen.width, Screen.height);
	
	private Client userClient;

	private string text1 = "waiting...";
	private string text2 = "waiting...";
	void OnGUI() {
		if(windowOn)
			GUI.Window (0, windowRect, windowFunction, "Score");
		GUI.color = Color.red;
	}
	
	void Update(){
		if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.JoystickButton6)) {
			windowOn = true;
		}
		else if (Input.GetKeyUp(KeyCode.Q) || Input.GetKeyUp(KeyCode.JoystickButton6))
		{
			windowOn = false;
		}
		if (Input.GetKeyDown (KeyCode.Z)) {
			updateScore("Charles", 1);
			updateScore("Tyler", 2);
		}

	}
	void windowFunction (int windowID) {
		GUI.Label (new Rect(Screen.width/3, 8 * Screen.height/100, Screen.width/5, Screen.height/8), "1. " + text1);
		GUI.Label (new Rect(Screen.width/3, 35 * Screen.height/100, Screen.width/5, Screen.height/8), "2. " + text2);
	}
	public void deleteScore(string username){
		if (text1 == username) {
			text1 = "Player DC";
		} else if (text2 == username) {
			text2 = "Player DC";
		}
	}
	public void updateScore(string username, int rank)
	{
		switch(rank){
		case 1:
			text1 = username;
			break;
		case 2:
			text2 = username;
			break;
		}
	}
	public void updateScoreFinal(string username, int rank)
	{
		switch(rank){
		case 1:
			text1 = username;
			break;
		case 2:
			text2 = username;
			break;
		}
		windowOn = true;
	}
}
