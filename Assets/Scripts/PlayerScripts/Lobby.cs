﻿using UnityEngine;
using System.Collections;

public class Lobby : MonoBehaviour {

	private bool windowOn = false;
	
	private Rect windowRect = new Rect(0, 0, Screen.width, Screen.height);

	public GUIStyle myStyle;

	private Client userClient;

	private bool playerIsReady = false;

	private string text1 = "waiting...";
	private string text2 = "waiting...";
	private string text3 = "waiting...";
	private string text4 = "waiting...";

	private string ready1 = "Not Ready";
	private string ready2 = "Not Ready";
	private string ready3 = "Not Ready";
	private string ready4 = "Not Ready";

	private string readyButton = "Ready";

	private Rect chatRect = new Rect(Screen.width/2 + 100, Screen.height / 5, 300, 250);
	private string messBox = "", messageToSend = string.Empty, user = "";

	private Client client;
	
	void Start(){
		client = (Client)FindObjectOfType (typeof(Client));
	}

	void OnGUI() {
		if (windowOn) {
			GUILayout.Box(messBox, GUILayout.Height(350));
			GUI.Window (0, windowRect, windowFunction, "Lobby", myStyle);
		}
		GUI.color = Color.red;
	}

	// Update is called once per frame
	void Update () {
	}

	void windowFunction (int windowID) {
		messageToSend = GUI.TextField (new Rect (Screen.width/2 + 100, Screen.height / 14, 250, Screen.height / 10), messageToSend, 100);
		if (GUI.Button (new Rect (Screen.width / 2 + 350, Screen.height / 14, 50, Screen.height / 10), "Send")) {
			//SendMessage(messageToSend);
			client.sendMessage(messageToSend);
		}
		GUI.Label (new Rect(Screen.width/3, 8 * Screen.height/100, Screen.width/5, Screen.height/8), "1. " + text1);
		GUI.Label (new Rect(Screen.width/3, 32 * Screen.height/100, Screen.width/5, Screen.height/8), "2. " + text2);
		GUI.Label (new Rect(Screen.width/3, 59 * Screen.height/100, Screen.width/5, Screen.height/8), "3. " + text3);
		GUI.Label (new Rect(Screen.width/3, 86 * Screen.height/100, Screen.width/5, Screen.height/8), "4. " + text4);
		GUI.Label (new Rect((Screen.width/2) + 5, 8 * Screen.height/100, Screen.width/5, Screen.height/8), ready1);
		GUI.Label (new Rect((Screen.width/2) + 5, 32 * Screen.height/100, Screen.width/5, Screen.height/8), ready2);
		GUI.Label (new Rect((Screen.width/2) + 5, 59 * Screen.height/100, Screen.width/5, Screen.height/8), ready3);
		GUI.Label (new Rect((Screen.width/2) + 5, 86 * Screen.height/100, Screen.width/5, Screen.height/8), ready4);
		if (GUI.Button (new Rect (Screen.width / 2 + 250, 4 * Screen.height / 5, Screen.width / 8, Screen.height / 8), readyButton)) {
			if(!playerIsReady){
				client.playerReady();
				readyButton = "Not Ready";
				playerIsReady = true;
			}
			else{
				client.playerUnready();
				readyButton = "Ready";
				playerIsReady = false;
			}

		}

		GUI.TextField (chatRect, messBox);
	}
	public void connect (int playerNumber, string playerName){
		switch (playerNumber) {
		case 1:
			text1 = playerName;
			break;
		case 2:
			text2 = playerName;
			break;
		case 3:
			text3 = playerName;
			break;
		case 4:
			text4 = playerName;
			break;
		}
		printMessage (playerName, "has connected");
	}
	public void printMessage(string nameSent, string mess)
	{
		messBox = nameSent + ": " + mess + "\n" + messBox;
		messageToSend = "";
	}

	public void bitchAssNigga()
	{
		windowOn = false;
		Debug.Log ("Came in here but");
	}

	public void showWindow()
	{
		windowOn = true;
	}

	public void playerReady(int playerNumber)
	{
		switch (playerNumber) {
		case 1:
			ready1 = "Ready";
			break;
		case 2:
			ready2 = "Ready";
			break;
		case 3:
			ready3 = "Ready";
			break;
		case 4:
			ready4 = "Ready";
			break;
		}
	}

	public void playerUnready(int playerNumber)
	{
		switch (playerNumber) {
		case 1:
			ready1 = "Not Ready";
			break;
		case 2:
			ready2 = "Not Ready";
			break;
		case 3:
			ready3 = "Not Ready";
			break;
		case 4:
			ready4 = "Not Ready";
			break;
		}
	}
}
