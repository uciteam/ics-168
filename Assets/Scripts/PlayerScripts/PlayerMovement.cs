﻿using UnityEngine;
using System.Collections;
using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text; 

public class PlayerMovement : MonoBehaviour {
	
	public float speed;

	private float currentX = 0f;
	private float currentY = 0f;

	public bool hasBeenInstanced = false;
	public GameObject bomb;
	
	private int hitPoints = 3;


	private StateObject sendObject;
	private int playerNumber;
	private float networkTimer = 33.0f;
	private bool allowedToSendMessage = true;
	private string session;

	private bool alive = true;
	private bool positionUpdated = false;
	private Vector3 initialPositon;

	void Start(){
	}
	void Update(){

		networkTimer -= (Time.deltaTime*1000);
		currentX = Input.GetAxis ("Horizontal");
		currentY = Input.GetAxis ("Vertical");
		GetComponent<Rigidbody2D>().velocity = new Vector2 (currentX * speed, currentY * speed);
		//Debug.Log (networkTimer);
		if (positionUpdated) {
			transform.position = initialPositon;
			positionUpdated = false;
			Debug.Log("CAME IN THIS FUCK");
		}
		if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.JoystickButton0)){
			var height = GetComponent<Renderer>().bounds.size.y / 4.0f;
			Vector3 position = new Vector3(transform.localPosition.x, transform.localPosition.y - height);
			//Instantiate(bomb, position, Quaternion.identity);
			Send(sendObject.workSocket, "{type: 'bomb', user: {session: '" + session + "'}, bomb: {xPos:'"+position.x+"', yPos: '"+ position.y+"', timer: '"+5.0f+"'}}<EOF>", sendObject);
			//sendObject.sendDone.WaitOne(5000);
		}
		if (checkPlayerMoving() && hasBeenInstanced && allowedToSendMessage) {
			Send(sendObject.workSocket, "{type: 'playerUpdate', user: {session: '" + session + "', player: {playerNumber: '" + playerNumber + "', xPos: '" + transform.localPosition.x + "', yPos: '" + transform.localPosition.y + "', alive: '" + alive + "'}}}<EOF>", sendObject);
			//sendObject.sendDone.WaitOne(5000);
			allowedToSendMessage = false;
		}
		if (networkTimer <= 0) {
			allowedToSendMessage = true;
			networkTimer = 33.0f;
		}

	}
	public void AddDamage(){
		Debug.Log ("Damage Taken");
		hitPoints -= 1;
		if (hitPoints <= 0) {
			alive = false;
			Send(sendObject.workSocket, "{type: 'playerUpdate', user: {session: '" + session + "', player: {playerNumber: '" + playerNumber + "', xPos: '" + transform.localPosition.x + "', yPos: '" + transform.localPosition.y + "', alive: '" + alive + "'}}}<EOF>", sendObject);
			Destroy(gameObject);
			//Destroy(this);
		}
	}

	private bool checkPlayerMoving(){
		if (currentX != 0 || currentY != 0){
			return true;
		}
		return false;
	}
	public void setSend(StateObject objectSent, int playerNumberPassed, string sessionPassed, Vector3 positionPassed){
		sendObject = objectSent;
		playerNumber = playerNumberPassed;
		session = sessionPassed;
		hasBeenInstanced = true;
		initialPositon = positionPassed;
		positionUpdated = true;
	}
	private void Send(Socket client, String data, StateObject so){
		byte[] byteData = Encoding.ASCII.GetBytes(data);
		try{client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), so);}
		catch(Exception e){
			Debug.Log(e.ToString());
		}
	}
	private static void SendCallback(IAsyncResult ar){
		try{
			StateObject so = (StateObject) ar.AsyncState;
			Socket client = so.workSocket;
			int bytesSent = client.EndSend(ar);
			//Debug.Log("Sent " + bytesSent + " bytes to server.");
			so.sendDone.Set();
		}
		catch (Exception e){
			Debug.Log(e.ToString());
		}
	}
}