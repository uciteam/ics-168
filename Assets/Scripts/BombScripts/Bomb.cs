﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour {

	public float timer = 5.0f;

	private float internalTimer;
	private GameObject explosionSphere;

	private bool explosionHappening = false;
	private float explosionObjectTimer = 0.25f;
	
	public AudioSource sounds;
	// Use this for initialization
	void Start () {
		internalTimer = 0.0f;
	}
	
	// Update is called once per frame
	void Update () {
		internalTimer += Time.deltaTime;
		if (explosionHappening) {
			explosionObjectTimer -= Time.deltaTime;
			if(explosionObjectTimer <= 0){
				Destroy(explosionSphere.gameObject);
				Destroy(this);
			}
		}
		else if (internalTimer >= timer) {
			ExplosionDamage(transform.position, 10.0f);
		}
	}
	void OnDestroy(){
		sounds = GetComponent<AudioSource>();
		sounds.Play();
	}

	void ExplosionDamage(Vector2 center, float radius) {

		Collider2D[] hitColliders = Physics2D.OverlapCircleAll(center, radius);
		createExplosion (10.0f);
		GetComponent<Renderer> ().enabled = false;
		int i = 0;
		while (i < hitColliders.Length) {
			hitColliders[i].SendMessage("AddDamage", SendMessageOptions.DontRequireReceiver);
			i++;
		}
	}

	void createExplosion(float radius){
		explosionSphere = GameObject.CreatePrimitive (PrimitiveType.Sphere);
		explosionSphere.transform.position = transform.position;
		explosionSphere.transform.localScale = new Vector3(radius * 2, radius * 2, -0.1f);
		explosionSphere.GetComponent<Renderer> ().material.color = Color.yellow;
		explosionHappening = true;
	}
}
