Authors:

* Tyler Hogan: 72358582
* Dante Garcia: 30022760
* Charles Pasiliao: 41515903


Controls:
Movement -- W, A, S, D
Place Bomb -- Spacebar
View Leaderboard -- Hold Q


Instructions:
1) Launch the server at "Server\Server\bin\Debug\Server.exe"
2) Open "Assets\Scripts\Networking\Client.cs" and change your IP address on line 64
3) Open scene "MainGame.unity" in "Assets\Scenes" using Unity
4) Start the game!
5) Repeat steps 2-3 for another computer (player)
6) Login with any username / password, using any session name
7) Press ready when you are ready. All players need to be ready for the game to start. Oh, you can chat as well!
8) Repeat step 6 and 7 for as many different sessions as you want


Notes and Comments for Arthur:
1) The server is locked at 2 players for now. You can change this
   by opening the server project and changing line 80. Note that
   over 2 players hasn't been fully tested.
2) Since we fixed the lag problems in our code, we are actually seeing
   real networking problems! Some packets get dropped, especially bomb
   placements when lots of other things are happening at the same time.
3) Animations for the other players (not yours) are coming shortly!
4) Play with sound :D
