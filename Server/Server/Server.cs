﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;



/* Game Classes
 * These classes are used for storing, deserializing, and sending
 * JSON. They keep track of players, bombs, and users in any given
 * session of the game.
 */

public class User
{
    [JsonProperty]
    internal string name;
    [JsonProperty]
    internal string password;
    [JsonProperty]
    internal Player player;
    [JsonProperty]
    internal string session;
    [JsonProperty]
    internal bool ready;
    internal Socket client;
}

public class Player
{
    [JsonProperty]
    internal int playerNumber;
    [JsonProperty]
    internal float xPos;
    [JsonProperty]
    internal float yPos;
    [JsonProperty]
    internal bool alive = true;
    [JsonProperty]
    internal int score = 0;
}

public class Bomb
{
    [JsonProperty]
    internal float xPos;
    [JsonProperty]
    internal float yPos;
    [JsonProperty]
    internal int timer;
}



/* Server Classes
 * These classes are used for handling clients and their connection
 * objects, as well as the main server for the game itself.
 */

public class StateObject
{
    // Client  socket.
    public Socket workSocket = null;
    // Size of receive buffer.
    public const int BufferSize = 1024;
    // Receive buffer.
    public byte[] buffer = new byte[BufferSize];
    // Received data string.
    public StringBuilder sb = new StringBuilder();
}

public class AsynchronousSocketListener
{
    private static String[] STRING_SEPARATORS = new String[] { "<EOF>" };
    private static int MAX_PLAYERS = 2;

    // Thread signal.
    public static SQLiteConnection dbConnection;
    public static AutoResetEvent allDone = new AutoResetEvent(false);
    public static Socket serverListener;
    public static Dictionary<String, List<User>> sessions = new Dictionary<String, List<User>>();
    public static List<Socket> clients = new List<Socket>();

    public AsynchronousSocketListener() {}



    /* Server Callbacks
     * All of these methods related to the server handling the connections
     * to all of it's clients.
     */

    public static void StartListening()
    {
        // Data buffer for incoming data.
        byte[] bytes = new Byte[1024];

        //Listen to external IP address
        IPHostEntry ipHostInfo = Dns.GetHostEntry(Dns.GetHostName());
        IPAddress ipAddress = ipHostInfo.AddressList[2];
        IPEndPoint localEndPoint = new IPEndPoint(ipAddress, 11000);

        // Create a TCP/IP socket.
        serverListener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);

        // Bind the socket to the local endpoint and listen for incoming connections.
        try
        {
            serverListener.Bind(localEndPoint);
            serverListener.Listen(100);

            // Socket loop.
            while (true)
            {
                // Start an asynchronous socket to listen for connections.
                Console.WriteLine("Waiting for a connection...\n");
                serverListener.BeginAccept(new AsyncCallback(AcceptCallback), serverListener);
                // Wait until a connection is made before continuing.
                allDone.WaitOne();
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString() + "\n");
        }
        Console.WriteLine("\nPress ENTER to continue...");
        Console.Read();
    }

    public static void AcceptCallback(IAsyncResult ar)
    {
        // Signal the main thread to continue.
        allDone.Set();

        // Get the socket that handles the client request.
        Socket listener = (Socket)ar.AsyncState;
        Socket handler = listener.EndAccept(ar);

        // Create the state object.
        StateObject state = new StateObject();
        state.workSocket = handler;

        // Games have bidirectional communication (as opposed to request/response)
        // So I need to store all clients sockets so I can send them messages later
        clients.Add(handler);
        Console.WriteLine("Accepted Connection!\n");
        handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
    }

    public static void ReadCallback(IAsyncResult ar)
    {
        String content = String.Empty;
        // Retrieve the state object and the handler socket from the asynchronous state object.
        StateObject state = (StateObject)ar.AsyncState;
        Socket handler = state.workSocket;

        int bytesRead = 0;
        try
        { 
            // Read data from the client socket. 
            bytesRead = handler.EndReceive(ar);
        }
        catch
        {
            DisconnectPlayer(handler);
            return;
        }

        if (bytesRead > 0)
        {
            // There might be more data, so store the data received so far.
            state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

            // Check for end-of-file tag. If it is not there, read more data.
            content = state.sb.ToString();
            if (content.IndexOf("<EOF>") > -1)
            {
                Console.WriteLine(content);
                JObject parsedJson;
                try
                {
                    content = content.Split(AsynchronousSocketListener.STRING_SEPARATORS, StringSplitOptions.RemoveEmptyEntries)[0];
                    parsedJson = JObject.Parse(content);
                }
                catch
                {
                    return;
                }

                User user;
                User existingUser;
                switch (parsedJson["type"].ToString())
                {
                    // When a new player attempts to join a session, this message is passed.
                    case "login":
                        // Handle what the client sent to the server.
                        user = JsonConvert.DeserializeObject<User>(parsedJson["user"].ToString());

                        List<User> usersInSession;
                        sessions.TryGetValue(user.session, out usersInSession);

                        if (usersInSession != null && usersInSession.FindAll(x => x.client != null).Count >= AsynchronousSocketListener.MAX_PLAYERS)
                        {
                            // If the session is full, alert the client and close their connection.
                            SendGameSessionFull(handler);
                            CloseSocket(handler);
                            return;
                        }
                        else
                        {
                            if (!CreateOrLoginUser(user))
                            {
                                // Reject the user.
                                SendIncorrectPassword(handler);
                                CloseSocket(handler);
                                return;
                            }

                            // Attach the client socket to the user.
                            user.client = clients.Find(socket => socket == state.workSocket);
                            if (usersInSession == null)
                            {
                                // Check if there are users in the current session, and create a new session if noone is there.
                                sessions[user.session] = new List<User>();
                                Console.WriteLine("New session created: {0}\n", user.session);
                            }

                            // Create a new player for the user if he has joined for the first time.
                            CreatePlayer(user);
                            sessions[user.session].Add(user);
                            SendPlayerConnect(user);
                        }
                        break;
                    // When a player sends a chat message in the lobby.
                    case "chat":
                        user = JsonConvert.DeserializeObject<User>(parsedJson["user"].ToString());
                        SendChat(user, parsedJson["message"].ToString());
                        break;
                    // When a player is in the lobby and presses ready.
                    case "playerReady":
                        user = JsonConvert.DeserializeObject<User>(parsedJson["user"].ToString());
                        existingUser = sessions[user.session].Find(x => x.name == user.name);
                        existingUser.ready = true;
                        SendPlayerReady(existingUser);
                        if (sessions[user.session].FindAll(x => x.client != null && x.ready).Count >= AsynchronousSocketListener.MAX_PLAYERS)
                            // Signal all the clients to start the game.
                            SendStartGame(user.session);
                        break;
                    // When a player is in the lobby and presses unready.
                    case "playerUnready":
                        user = JsonConvert.DeserializeObject<User>(parsedJson["user"].ToString());
                        existingUser = sessions[user.session].Find(x => x.name == user.name);
                        existingUser.ready = false;
                        SendPlayerUnready(existingUser);
                        break;
                    // When a player moves or dies, this message is passed.
                    case "playerUpdate":
                        user = JsonConvert.DeserializeObject<User>(parsedJson["user"].ToString());
                        UpdatePlayer(user);
                        if (!GameStillRunning(user.session))
                        {
                            EndGame(user.session);
                            return;
                        }
                        break;
                    case "playerDisconnect":
                        DisconnectPlayer(handler);
                        break;
                    // When a player places a bomb, this message is passed.
                    case "bomb":
                        user = JsonConvert.DeserializeObject<User>(parsedJson["user"].ToString());
                        Bomb bomb = JsonConvert.DeserializeObject<Bomb>(parsedJson["bomb"].ToString());
                        SendBomb(user, bomb);
                        break;
                }

                // Setup a new state object
                StateObject newstate = new StateObject();
                newstate.workSocket = handler;

                try
                {
                    // Call BeginReceive with a new state object.
                    handler.BeginReceive(newstate.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), newstate);
                }
                catch
                {
                    // If something goes wrong, the player probably disconnected without telling us.
                    DisconnectPlayer(handler);
                }

            }
            else
                // Not all data received. Get more.
                handler.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(ReadCallback), state);
        }
    }

    private static void Send(Socket handler, String data)
    {
        // Convert the string data to byte data using ASCII encoding.
        byte[] byteData = Encoding.ASCII.GetBytes(data);

        try
        {
            // Begin sending the data to the remote device.
            handler.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), handler);
        }
        catch
        {
            // If something goes wrong, the client probably disconnected without telling us.
            DisconnectPlayer(handler);
        }
    }

    private static void SendCallback(IAsyncResult ar)
    {
        try
        {
            // Retrieve the socket from the state object.
            Socket handler = (Socket)ar.AsyncState;

            // Complete sending the data to the remote device.
            int bytesSent = handler.EndSend(ar);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.ToString() + "\n");
        }
    }

    private static void CloseSocket(Socket handler)
    {
        clients.Remove(handler);
        handler.Shutdown(SocketShutdown.Both);
        handler.Close();
    }



    /* Gamestate Helper Methods
     * These methods are all game related, and update players / bombs on the
     * server so that it can keep all clients in sync.
     */

    private static void CreatePlayer(User user)
    {
        Player player = new Player();
        player.playerNumber = sessions[user.session].Count + 1;
        switch (player.playerNumber)
        {
            case 1:
                player.xPos = -65;
                player.yPos = 38;
                break;
            case 2:
                player.xPos = 17;
                player.yPos = 38;
                break;
            case 3:
                player.xPos = -65;
                player.yPos = -30;
                break;
            case 4:
                player.xPos = 17;
                player.yPos = -30;
                break;
        }
        user.player = player;
    }

    private static void UpdatePlayer(User user)
    {
        User userToUpdate = sessions[user.session].Find(x => x.player.playerNumber == user.player.playerNumber);
        userToUpdate.player.xPos = user.player.xPos;
        userToUpdate.player.yPos = user.player.yPos;
        userToUpdate.player.alive = user.player.alive;
        if (!userToUpdate.player.alive)
        {
            userToUpdate.player.score = sessions[userToUpdate.session].FindAll(x => x.client != null && x.player.alive).Count + 1;
        }
        SendPlayerUpdate(userToUpdate);
    }

    private static void DisconnectPlayer(Socket handler)
    {
        User userToRemove;
        foreach (List<User> userList in sessions.Values)
        {
            userToRemove = userList.Find(x => x.client == handler);
            if (userToRemove != null)
            {
                sessions[userToRemove.session].Remove(userToRemove);
                CloseSocket(userToRemove.client);
                userToRemove.client = null;
                SendPlayerDisconnect(userToRemove);
                if (sessions[userToRemove.session].FindAll(x => x.client != null && x.player.alive).Count <= 1)
                    EndGame(userToRemove.session);
            }
        }
    }

    private static bool GameStillRunning(String session)
    {
        List<User> aliveUsers = sessions[session].FindAll(x => x.player.alive);
        if (sessions[session].Count >= AsynchronousSocketListener.MAX_PLAYERS && aliveUsers.Count <= 1)
            return false;
        return true;
    }

    private static void EndGame(String session)
    {
        User lastUser = sessions[session].Find(x => x.client != null && x.player.score == 0);
        if (lastUser != null)
            lastUser.player.score = 1;
        // If everyone has died, send the end game message.
        SendEndGame(session);

        foreach (User sessionUser in sessions[session].FindAll(x => x.client != null))
        {
            CloseSocket(sessionUser.client);
            sessions[session].Clear();
        }
    }



    /* JSON Send Methods
     * The following methods are all helpers for sending our key JSON messages to
     * all of the clients.
     */

    private static void SendChat(User user, String message)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("chat");
            writer.WritePropertyName("user");
            writer.WriteStartObject();
            writer.WritePropertyName("name");
            writer.WriteValue(user.name);
            writer.WriteEndObject();
            writer.WritePropertyName("message");
            writer.WriteValue(message);
            writer.WriteEndObject();
        }
        SendToAllClients(user.session, sb.ToString());
        Console.WriteLine("Message from {0}: {1}", user.name, message);
    }

    private static void SendStartGame(String session)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("gamestate");
            writer.WritePropertyName("state");
            writer.WriteValue("started");
            writer.WritePropertyName("users");
            writer.WriteStartArray();
            foreach (User user in sessions[session])
            {
                writer.WriteStartObject();
                writer.WritePropertyName("name");
                writer.WriteValue(user.name);
                RenderPlayer(writer, user.player);
                writer.WriteEndObject();
            }
            writer.WriteEndArray();
            writer.WriteEndObject();
        }
        SendToAllClients(session, sb.ToString());
        Console.WriteLine("Game has started on session: {0}\n", session);
    }

    private static void SendEndGame(String session)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("gamestate");
            writer.WritePropertyName("state");
            writer.WriteValue("ended");

            writer.WritePropertyName("users");
            writer.WriteStartArray();
            foreach (User user in sessions[session])
            {
                writer.WriteStartObject();
                writer.WritePropertyName("name");
                writer.WriteValue(user.name);
                RenderPlayer(writer, user.player);
                writer.WriteEndObject();
            }

            writer.WriteEndObject();
        }
        SendToAllClients(session, sb.ToString());
        Console.WriteLine("Game has ended on session: {0}\n", session);
    }

    private static void SendGameSessionFull(Socket handler)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("error");
            writer.WritePropertyName("status");
            writer.WriteValue("gameSessionFull");
            writer.WriteEndObject();
        }

        Send(handler, sb.ToString() + "<EOF>");
    }

    private static void SendIncorrectPassword(Socket handler)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("error");
            writer.WritePropertyName("status");
            writer.WriteValue("incorrectPassword");
            writer.WriteEndObject();
        }

        Send(handler, sb.ToString() + "<EOF>");
    }

    private static void SendPlayerConnect(User user)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("playerConnect");
            writer.WritePropertyName("user");
            writer.WriteStartObject();
            writer.WritePropertyName("name");
            writer.WriteValue(user.name);
            RenderPlayer(writer, user.player);
            writer.WriteEndObject();
            writer.WritePropertyName("existingUsers");
            writer.WriteStartArray();
            foreach (User existingUser in sessions[user.session].FindAll(x => x.name != user.name))
            {
                writer.WriteStartObject();
                writer.WritePropertyName("name");
                writer.WriteValue(existingUser.name);
                writer.WritePropertyName("ready");
                writer.WriteValue(existingUser.ready);
                RenderPlayer(writer, existingUser.player);
                writer.WriteEndObject();
            }
            writer.WriteEndArray();
            writer.WriteEndObject();
        }
        SendToAllClients(user.session, sb.ToString());
        Console.WriteLine("New player, \"{0}\", has joined session: {1}\n", user.name, user.session);
    }

    private static void SendPlayerReady(User user)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("playerReady");
            writer.WritePropertyName("user");
            writer.WriteStartObject();
            writer.WritePropertyName("name");
            writer.WriteValue(user.name);
            RenderPlayer(writer, user.player);
            writer.WriteEndObject();
            writer.WriteEndObject();
        }
        SendToAllClients(user.session, sb.ToString());
        Console.WriteLine("User has readied: {0}", user.name);
    }

    private static void SendPlayerUnready(User user)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("playerUnready");
            writer.WritePropertyName("user");
            writer.WriteStartObject();
            writer.WritePropertyName("name");
            writer.WriteValue(user.name);
            RenderPlayer(writer, user.player);
            writer.WriteEndObject();
            writer.WriteEndObject();
        }
        SendToAllClients(user.session, sb.ToString());
        Console.WriteLine("User has unreadied: {0}", user.name);
    }

    private static void SendPlayerUpdate(User user)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("playerUpdate");
            RenderPlayer(writer, user.player);
            writer.WriteEndObject();
        }
        SendToAllClients(user.session, sb.ToString());
    }

    private static void SendPlayerDisconnect(User user)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("playerDisconnect");
            writer.WritePropertyName("user");
            writer.WriteStartObject();
            writer.WritePropertyName("name");
            writer.WriteValue(user.name);
            writer.WritePropertyName("player");
            writer.WriteStartObject();
            writer.WritePropertyName("playerNumber");
            writer.WriteValue(user.player.playerNumber);
            writer.WriteEndObject();
            writer.WriteEndObject();
            writer.WriteEndObject();
        }

        SendToAllClients(user.session, sb.ToString());
    }

    private static void SendBomb(User user, Bomb bomb)
    {
        StringBuilder sb = new StringBuilder();
        StringWriter sw = new StringWriter(sb);

        using (JsonWriter writer = new JsonTextWriter(sw))
        {
            writer.Formatting = Formatting.None;
            writer.WriteStartObject();
            writer.WritePropertyName("type");
            writer.WriteValue("bomb");
            RenderBomb(writer, bomb);
            writer.WriteEndObject();
        }
        SendToAllClients(user.session, sb.ToString());
    }

    private static void SendToAllClients(String session, String message)
    {
        foreach (Socket client in sessions[session].FindAll(x => x.client != null).Select(x => x.client))
        {
            try
            {
                Send(client, message + "<EOF>");
            }
            catch
            {
                // If something goes wrong, the client probably disconnected without telling us.
                DisconnectPlayer(client);
            }
        }
    }



    /* JSON Helper Methods
     * The following methods all help build JSON objects which are used
     * regularly to send information to the clients. Each object has it's
     * own helper.
     */

    private static void RenderPlayer(JsonWriter writer, Player player)
    {
        writer.WritePropertyName("player");
        writer.WriteStartObject();
        writer.WritePropertyName("playerNumber");
        writer.WriteValue(player.playerNumber);
        writer.WritePropertyName("xPos");
        writer.WriteValue(player.xPos);
        writer.WritePropertyName("yPos");
        writer.WriteValue(player.yPos);
        writer.WritePropertyName("alive");
        writer.WriteValue(player.alive);
        writer.WritePropertyName("score");
        writer.WriteValue(player.score);
        writer.WriteEndObject();
    }

    private static void RenderBomb(JsonWriter writer, Bomb bomb)
    {
        writer.WritePropertyName("bomb");
        writer.WriteStartObject();
        writer.WritePropertyName("xPos");
        writer.WriteValue(bomb.xPos);
        writer.WritePropertyName("yPos");
        writer.WriteValue(bomb.yPos);
        writer.WritePropertyName("timer");
        writer.WriteValue(bomb.timer);
        writer.WriteEndObject();
    }



    /* Main Application
     * This is the main loop which starts the server and initializes
     * the database if it doesn't already exist.
     */

    private static bool CreateOrLoginUser(User user)
    {
        // Check if the User exists already or not.
        SQLiteCommand commandUserLookup = new SQLiteCommand("SELECT COUNT(*) FROM users WHERE name='" + user.name + "'", dbConnection);
        int countUserCheck = Convert.ToInt32(commandUserLookup.ExecuteScalar());
        if (countUserCheck == 0)
        {
            // Create a new user in the database.
            SQLiteCommand commandCreateUser = new SQLiteCommand("INSERT INTO users (name, password) VALUES ('" + user.name + "', '" + user.password + "')", dbConnection);
            commandCreateUser.ExecuteNonQuery();
            return true;
        }
        else
        {
            // Check if the user's password matches the given password.
            SQLiteCommand commandUserPasswordCheck = new SQLiteCommand("SELECT COUNT(*) FROM users WHERE password='" + user.password + "'", dbConnection);
            int countPasswordCheck = Convert.ToInt32(commandUserPasswordCheck.ExecuteScalar());
            if (countPasswordCheck == 0)
                return false;
            return true;
        }
    }

    public static void InitializeDatabase()
    {
        if (!File.Exists("Bomberman.sqlite"))
        {
            // Create a SQLite database file for storing users.
            SQLiteConnection.CreateFile("Bomberman.sqlite");
            dbConnection = new SQLiteConnection("Data Source=Bomberman.sqlite;Version=3;");
            dbConnection.Open();
            SQLiteCommand commandCreateUsers = new SQLiteCommand("CREATE TABLE users (name varchar(20), password varchar(20))", dbConnection);
            commandCreateUsers.ExecuteNonQuery();
        }
        else
        {
            // If a SQLite database exists, just open a connection to it.
            dbConnection = new SQLiteConnection("Data Source=Bomberman.sqlite;Version=3;");
            dbConnection.Open();
        }
    }

    public static int Main(String[] args)
    {
        // Create a SQLite database file for the server if one does not already exist.
        InitializeDatabase();
        // Start the server and begin listening for clients.
        StartListening();
        return 0;
    }
}
